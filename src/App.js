import React from 'react';
import InstanceSearch from './components/InstanceSearch';
import InstanceHistory from './components/InstanceHistory';
import InstanceSuggestions from './components/InstanceSuggestions';
import { Container, Tabs, Tab } from '@mui/material';

function App() {
  const [tab, setTab] = React.useState(0);

  const handleChange = (event, newValue) => {
    setTab(newValue);
  };

  return (
    <Container>
      <Tabs value={tab} onChange={handleChange}>
        <Tab label="Instance Search" />
        <Tab label="Instance History" />
        <Tab label="Instance Suggestions" />
      </Tabs>
      {tab === 0 && <InstanceSearch />}
      {tab === 1 && <InstanceHistory />}
      {tab === 2 && <InstanceSuggestions />}
    </Container>
  );
}

export default App;
