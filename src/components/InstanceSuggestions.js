import React, { useState } from 'react';
import axios from 'axios';
import { TextField, Button, Table, TableBody, TableCell, TableHead, TableRow, Select, MenuItem } from '@mui/material';

const InstanceSuggestions = () => {
  const [region, setRegion] = useState('');
  const [cpu, setCpu] = useState('');
  const [memory, setMemory] = useState('');
  const [results, setResults] = useState([]);

  const handleSearch = async () => {
    if (!region || !cpu || !memory) {
      alert("Please fill in all fields");
      return;
    }

    try {
      console.log(`Searching suggestions for region: ${region}, cpu: ${cpu}, memory: ${memory}`);
      const response = await axios.get('http://127.0.0.1:8000/instances/suggestions', {
        params: { region, cpu: Number(cpu), memory: Number(memory) }
      });
      console.log(response.data);
      setResults(response.data);
    } catch (error) {
      console.error("Error fetching instance suggestions", error);
    }
  };

  return (
    <div>
      <Select value={region} onChange={e => setRegion(e.target.value)} displayEmpty>
        <MenuItem value=""><em>Select Region</em></MenuItem>
        <MenuItem value="us-east-1">us-east-1</MenuItem>
        <MenuItem value="us-east-2">us-east-2</MenuItem>
        {/* Outras opções de região */}
      </Select>
      <TextField type="number" label="CPU" value={cpu} onChange={e => setCpu(e.target.value)} />
      <TextField type="number" label="Memory" value={memory} onChange={e => setMemory(e.target.value)} />
      <Button onClick={handleSearch}>Get Suggestions</Button>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Instance Type</TableCell>
            <TableCell>vCPU</TableCell>
            <TableCell>Memory GiB</TableCell>
            <TableCell>Savings over On-Demand</TableCell>
            <TableCell>Frequency of Interruption</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {results.map(instance => (
            <TableRow key={instance.id}>
              <TableCell>{instance.instance_type}</TableCell>
              <TableCell>{instance.vcpu}</TableCell>
              <TableCell>{instance.memory_gib}</TableCell>
              <TableCell>{instance.savings_over_on_demand}</TableCell>
              <TableCell>{instance.frequency_of_interruption}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
};

export default InstanceSuggestions;
