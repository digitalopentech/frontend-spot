Frontend
Clone o Repositório:

git clone https://github.com/usuario/repositorio.git
cd frontend

Instalar Dependências:

npm install  # Ou use `yarn install` se preferir

Executar o Frontend:

npm start  # Ou use `yarn start`


Utilização
Endpoints
GET /instances:

Parâmetros: region, cpu (opcional), memory (opcional), instance_type (opcional). Retorna: Lista de instâncias que correspondem aos critérios fornecidos.

GET /instances/history:

Parâmetros: region, instance_type. Retorna: Histórico de uma instância específica.

GET /instances/suggestions:

Parâmetros: region, cpu, memory. Retorna: Sugestões de instâncias com base nos critérios fornecidos.

Componentes React
InstanceHistory: Busca histórico de instâncias com base na região e tipo de instância.
InstanceSearch: Busca instâncias com base na região, CPU, memória e tipo de instância.
InstanceSuggestions: Sugere instâncias com base na região, CPU e memória.

Exemplo de Uso
Busca de Instâncias: Navegue até InstanceSearch no frontend. Selecione a região, insira os valores de CPU e memória (opcionalmente, o tipo de instância). Clique em "Search" para visualizar os resultados.

Histórico de Instâncias: Navegue até InstanceHistory no frontend. Selecione a região e insira o tipo de instância. Clique em "Search" para visualizar o histórico.

Sugestões de Instâncias: Navegue até InstanceSuggestions no frontend. Selecione a região, insira os valores de CPU e memória. Clique em "Get Suggestions" para visualizar as sugestões.